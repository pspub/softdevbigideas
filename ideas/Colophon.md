Title: Colophon

Index: Colophon

Body:

The following tools were used in the making of this site. 

* [Amazon][]
* [BBEdit][]
* [Bitbucket][]
* [Bootstrap][]
* [Byword][]
* [Equity][] & [Concourse][] fonts
* [Git][]
* [HTML5][]
* [iWisdom][]
* [Macintosh][mac]
* [Markdown][]
* [PSTextMerge][]
* [Tower][]

[amazon]: http://amzn.to/2bCdyyW
[bbedit]: http://www.barebones.com/products/bbedit/
[bitbucket]: https://bitbucket.org/
[bootstrap]: http://getbootstrap.com/
[byword]: https://bywordapp.com/
[concourse]: http://practicaltypography.com/concourse.html
[equity]: http://practicaltypography.com/equity.html
[git]: https://git-scm.com/
[html5]: https://www.w3.org/TR/html5/
[iwisdom]: http://www.powersurgepub.com/products/iwisdom/index.html
[mac]: http://www.apple.com/mac/
[markdown]: https://daringfireball.net/projects/markdown/
[PSTextMerge]: http://www.powersurgepub.com/products/pstextmerge/index.html
[tower]: https://www.git-tower.com/
